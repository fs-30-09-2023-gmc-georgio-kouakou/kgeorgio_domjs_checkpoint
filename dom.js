let products = document.querySelectorAll(".product");

let price = document.querySelectorAll(".product-price");
let less_qty = document.querySelectorAll(".product-qty-less");
let plus_qty = document.querySelectorAll(".product-qty-plus");
let qty = document.querySelectorAll(".product-qty");
let total = document.querySelectorAll(".total");
let addProduct = document.querySelectorAll(".add-product");
let qte_Panier = document.querySelectorAll(".notification");


for(const key in products){
    let product = products[key];

    let priceValue= price[key].value;
    let qtyValue = qty[key].value;
    let totalValue = total[key].value;
    let cartValue = addProduct[key].value;

    if(qty[key].addEventListener){
        qty[key].addEventListener("keyup",
            (event) => {
                total[key].value = priceValue*qty[key].value;
                console.log(total[key].value)
            }
        )
    }

    if(less_qty[key].addEventListener){
        less_qty[key].addEventListener("click",
            (event) => {
                qty[key].value--;
                total[key].value = priceValue*qty[key].value;
            }
        )
    }

    if(plus_qty[key].addEventListener){
        plus_qty[key].addEventListener("click",
            (event) => {
                qty[key].value++;
                total[key].value = priceValue*qty[key].value;
            }
        )
    }

    if(addProduct[key].addEventListener && qte_Panier){
        console.log(addProduct[key])
    }


}

